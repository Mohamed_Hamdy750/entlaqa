<?php

namespace App\Http\Resources\Industry;

use Illuminate\Http\Resources\Json\JsonResource;

class Stands extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'branch_id' => $this->branch_id,
        ];
    }
}

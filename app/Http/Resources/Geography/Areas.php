<?php

namespace App\Http\Resources\Geography;

use Illuminate\Http\Resources\Json\JsonResource;

class Areas extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'city_id' => $this->city_id,
        ];
    }
}

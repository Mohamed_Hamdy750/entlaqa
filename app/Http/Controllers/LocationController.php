<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Geography\Country;
use App\Models\Industry\Company;
use App\Http\Resources\Geography\Countries;
use App\Http\Resources\Industry\Companies;

class LocationController extends Controller
{
    public function index()
    {
        $countries = Countries::collection(Country::all());
        $companies = Companies::collection(Company::all());

        return view('locate', compact('countries', 'companies'));
    }
}

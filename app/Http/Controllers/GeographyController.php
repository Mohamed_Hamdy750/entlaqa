<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\Geography\Cities;
use App\Http\Resources\Geography\Areas;
use App\Models\Geography\Country;
use App\Models\Geography\City;

class GeographyController extends Controller
{
    public function getCity(Country $country)
    {
        $cities = Cities::collection($country->cities()->get());
        return response()->json($cities);
    }

    public function getArea(City $city)
    {
        $areas = Areas::collection($city->areas()->get());
        return response()->json($areas);
    }
}

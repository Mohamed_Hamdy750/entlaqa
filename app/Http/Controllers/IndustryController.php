<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\Industry\Stands;
use App\Http\Resources\Industry\Branches;
use App\Http\Resources\Industry\Brands;
use App\Models\Industry\Brand;
use App\Models\Industry\Branch;
use App\Models\Industry\Company;
use App\Models\Geography\Area;

class IndustryController extends Controller
{
    public function getBrand(Company $company)
    {
        $brands = Brands::collection($company->brands()->get());
        return response()->json($brands);
    }

    public function getBranchByBrandAndArea(Brand $brand, Area $area)
    {
        $branches = Branches::collection($brand->branches()->where('area_id', '=', $area->id)->get());
        return response()->json($branches);
    }

    public function getBranchByArea(Area $area)
    {
        $branches = Branches::collection($area->branches()->get());
        return response()->json($branches);
    }

    public function getBranchByBrand(Brand $brand)
    {
        $branches = Branches::collection($brand->branches()->get());
        return response()->json($branches);
    }

    public function getStand(Branch $branch)
    {
        $stands = Stands::collection($branch->stands()->get());
        return response()->json($stands);
    }
}

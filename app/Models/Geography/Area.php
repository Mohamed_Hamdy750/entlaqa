<?php

namespace App\Models\Geography;

use Illuminate\Database\Eloquent\Model;
use App\Models\Industry\Branch;

class Area extends Model
{
    protected $fillable = ['name', 'city_id'];

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function branches()
    {
        return $this->hasMany(Branch::class);
    }
}

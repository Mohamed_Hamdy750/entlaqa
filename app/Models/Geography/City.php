<?php

namespace App\Models\Geography;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'cities';
    protected $fillable = ['name', 'country_id'];

    public function country()
    {
        return $this->belongsTo(Country);
    }

    public function areas()
    {
        return $this->hasMany(Area::class);
    }
}

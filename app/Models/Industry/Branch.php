<?php

namespace App\Models\Industry;

use Illuminate\Database\Eloquent\Model;
use App\Models\Geography\Area;

class Branch extends Model
{
    protected $fillable = ['name', 'brand_id', 'area_id'];

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function area()
    {
        return $this->belongsTo(Area::class);
    }

    public function stands()
    {
        return $this->hasMany(Stand::class);
    }
}

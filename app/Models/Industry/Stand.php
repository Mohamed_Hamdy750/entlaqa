<?php

namespace App\Models\Industry;

use Illuminate\Database\Eloquent\Model;

class Stand extends Model
{
    protected $fillable = ['name', 'branch_id'];

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }
}

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <title>Entlaqa</title>

        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

        <style>
            .content-page{
                background: lemonchiffon;
                text-align: center;
                color: tan;
                font-family: 'Courier New';
                font-weight: bold;
            }
            .container{
                border-radius: 20px;
                margin-top: 50px;
                padding-bottom: 20px;
                background: tan;
                text-align: left;
                color:lemonchiffon;
                font-weight: normal;
            }

            .form-control{
                background: lemonchiffon;
            }

            .lds-dual-ring {
                display: inline-block;
                width: 10px;
                height: 10px;
            }
            .lds-dual-ring:after {
                content: " ";
                display: block;
                width: 20px;
                height: 20px;
                background-position: 50% 50%;
                margin-top: 26px;
                border-radius: 50%;
                border: 2px solid #fff;
                border-color: #fff transparent #fff transparent;
                animation: lds-dual-ring 1.2s linear infinite;
            }
            @keyframes lds-dual-ring {
                0% {
                    transform: rotate(0deg);
                }
                100% {
                    transform: rotate(360deg);
                }
            }

        </style>
    </head>

    <body class="content-page">
        <h1>Entlaqa Company</h1>

        <div class="container">
            <h3>Make Your Choice :</h3>
            <hr>
            <form action="" method="get" enctype="multipart/form-data">
                <div class="form-group col-md-12">
                        <label for="form_control_1">Country</label>
                        <select class="form-control" id="country" name="country_id">
                            <option value="">-Select Country-</option>
                            @foreach ($countries as $country)
                                <option value="{{$country->id}}">{{$country->name}}</option>
                            @endforeach
                        </select>
                </div>

                <div class="form-group col-md-5">
                        <label for="form_control_1">City</label>
                        <select class="form-control" id="city" name="city_id">
                                <option value="">-Select City-</option>
                        </select>
                </div>

                <div class="form-group col-md-1">
                    <div class="lds-dual-ring" id="city-spinner"></div>
                </div>

                <div class="form-group col-md-5">
                        <label for="form_control_1">Area</label>
                        <select class="form-control" id="area" name="area_id">
                                <option value="">-Select Area-</option>
                        </select>
                </div>

                <div class="form-group col-md-1">
                    <div class="lds-dual-ring" id="area-spinner"></div>
                </div>

                <div class="form-group col-md-5">
                        <label for="form_control_1">Company</label>
                        <select class="form-control" id="company" name="company_id">
                            <option value="">-Select Company-</option>
                            @foreach ($companies as $company)
                                <option value="{{$company->id}}">{{$company->name}}</option>
                            @endforeach
                        </select>
                </div>

                <div class="form-group col-md-1"></div>

                <div class="form-group col-md-5">
                        <label for="form_control_1">Brand</label>
                        <select class="form-control" id="brand" name="brand_id">
                                <option value="">-Select Brand-</option>
                        </select>
                </div>

                <div class="form-group col-md-1">
                    <div class="lds-dual-ring" id="brand-spinner"></div>
                </div>

                <div class="form-group col-md-5">
                        <label for="form_control_1">Branch</label>
                        <select class="form-control" id="branch" name="branch_id">
                                <option value="">-Select Branch-</option>
                        </select>
                </div>

                <div class="form-group col-md-1">
                    <div class="lds-dual-ring" id="branch-spinner"></div>
                </div>

                <div class="form-group col-md-5">
                        <label for="form_control_1">Stand</label>
                        <select class="form-control" id="stand" name="stand_id">
                                <option value="">-Select Stand-</option>
                        </select>
                </div>

                <div class="form-group col-md-1">
                    <div class="lds-dual-ring" id="stand-spinner"></div>
                </div>
            </form>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

        <script>
            var $country = $("#country");
            var $city = $("#city");
            var $area = $("#area");

            var $company = $("#company");
            var $brand = $("#brand");
            var $branch = $("#branch");
            var $stand = $("#stand");

            $(document).ready(function(){
                $('.lds-dual-ring').hide();
                $country.on('change', function(){
                    if($country.children("option:selected").val() != "")
                    {
                        $.ajax({
                            url: '/country/'+$country.children("option:selected").val()+'/cities',
                            type: 'get',
                            dataType: 'json',
                            success: function(response){
                                $('#city-spinner').show();
                                $city.empty();
                                $city.append(new Option("-Select City-", ""));
                                $area.empty();
                                $area.append(new Option("-Select Area-", ""));
                                $area.trigger('change');
                                $city.prop('disabled', true);
                                $area.prop('disabled', true);
                                for(i=0 ; i < response.length ; i++){
                                    var new_option = new Option(response[i].name, response[i].id);
                                    $(new_option).html(response[i].name);
                                    $city.append(new_option);
                                }
                                $city.prop('disabled', false);
                                $area.prop('disabled', false);
                                $('#city-spinner').hide();
                            }
                        });
                    }else{
                        $city.empty();
                        $city.append(new Option("-Select City-", ""));
                        $city.trigger('change');

                        $area.empty();
                        $area.append(new Option("-Select Area-", ""));
                        $area.trigger('change')
                    }
                });

                $city.on('change', function(){
                    if($city.children("option:selected").val() != "")
                    {
                        $.ajax({
                            url: '/city/'+$city.children("option:selected").val()+'/areas',
                            type: 'get',
                            dataType: 'json',
                            success: function(response){
                                $('#area-spinner').show();
                                $area.empty();
                                $area.append(new Option("-Select Area-", ""));
                                $area.trigger('change');

                                $area.prop('disabled', true);
                                for(i=0 ; i < response.length ; i++){
                                    var new_option = new Option(response[i].name, response[i].id);
                                    $(new_option).html(response[i].name);
                                    $area.append(new_option);
                                }
                                $area.prop('disabled', false);
                                $('#area-spinner').hide();
                            }
                        });
                    }else{
                        $area.empty();
                        $area.append(new Option("-Select Area-", ""));
                        $area.trigger('change');
                    }
                });

                $area.on('change', function(){
                    $('#branch-spinner').show();
                    if($area.children("option:selected").val() == "")
                    {
                        $branch.empty();
                        $branch.append(new Option("-Select Branch-", ""));
                        $branch.trigger('change');
                    }
                    getBranches();
                    $('#branch-spinner').hide();
                });

                $company.on('change', function(){
                    if($company.children("option:selected").val() != "")
                    {
                        $.ajax({
                            url: '/company/'+$company.children("option:selected").val()+'/brands',
                            type: 'get',
                            dataType: 'json',
                            success: function(response){
                                $('#brand-spinner').show();
                                $brand.empty();
                                $brand.append(new Option("-Select Brand-", ""));
                                $brand.trigger('change');

                                $branch.empty();
                                $branch.append(new Option("-Select Branch-", ""));

                                $stand.empty();
                                $stand.append(new Option("-Select Stand-", ""));

                                $brand.prop('disabled', true);
                                $branch.prop('disabled', true);
                                $stand.prop('disabled', true);
                                for(i=0 ; i < response.length ; i++){
                                    var new_option = new Option(response[i].name, response[i].id);
                                    $(new_option).html(response[i].name);
                                    $brand.append(new_option);
                                }
                                $brand.prop('disabled', false);
                                $branch.prop('disabled', false);
                                $stand.prop('disabled', false);
                                $('#brand-spinner').hide();
                            }
                        });
                    }else{
                        $brand.empty();
                        $brand.append(new Option("-Select Brand-", ""));
                        $brand.trigger('change');
                    }
                });

                $brand.on('change', function(){
                    $('#branch-spinner').show();
                    if($brand.children("option:selected").val() == "")
                    {
                        $branch.empty();
                        $branch.append(new Option("-Select Branch-", ""));
                        $branch.trigger('change');
                    }
                    getBranches();
                    $('#branch-spinner').hide();
                });

                $branch.on('change', function(){
                    if($branch.children("option:selected").val() != "")
                    {
                        $.ajax({
                            url: '/branch/'+$branch.children("option:selected").val()+'/stands',
                            type: 'get',
                            dataType: 'json',
                            success: function(response){
                                $('#stand-spinner').show();
                                $stand.empty();
                                $stand.append(new Option("-Select Stand-", ""));

                                $stand.prop('disabled', true);
                                for(i=0 ; i < response.length ; i++){
                                    var new_option = new Option(response[i].name, response[i].id);
                                    $(new_option).html(response[i].name);
                                    $stand.append(new_option);
                                }
                                $stand.prop('disabled', false);
                                $('#stand-spinner').hide();
                            }
                        });
                    }else{
                        $stand.empty();
                        $stand.append(new Option("-Select Stand-", ""));
                    }
                });

            });

            function getBranches()
            {
                url = '';
                if($area.children("option:selected").val() == "" && $brand.children("option:selected").val() != ""){
                    url = '/brand/'+$brand.children("option:selected").val()+'/branches';
                }else if($area.children("option:selected").val() != "" && $brand.children("option:selected").val() == ""){
                    url = '/area/'+$area.children("option:selected").val()+'/branches';
                }
                else if($area.children("option:selected").val() != "" && $brand.children("option:selected").val() != ""){
                    url = '/brand/'+$brand.children("option:selected").val()+'/area/'+$area.children("option:selected").val()+'/branches';
                }else{
                    $branch.empty();
                    $branch.append(new Option("-Select Branch-", ""));
                    $branch.trigger('change');
                }

                $.ajax({
                    url: url,
                    type: 'get',
                    dataType: 'json',
                    success: function(response){
                        $branch.empty();
                        $branch.append(new Option("-Select Branch-", ""));

                        $stand.empty();
                        $stand.append(new Option("-Select Stand-", ""));

                        $branch.prop('disabled', true);
                        $stand.prop('disabled', true);
                        for(i=0 ; i < response.length ; i++){
                            var new_option = new Option(response[i].name, response[i].id);
                            $(new_option).html(response[i].name);
                            $branch.append(new_option);
                        }
                        $branch.prop('disabled', false);
                        $stand.prop('disabled', false);
                    }
                });
            }
        </script>
    </body>
</html>

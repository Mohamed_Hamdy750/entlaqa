<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'LocationController@index');

Route::get('/country/{country}/cities','GeographyController@getCity');
Route::get('/city/{city}/areas','GeographyController@getArea');
Route::get('/company/{company}/brands','IndustryController@getBrand');
Route::get('/brand/{brand}/branches','IndustryController@getBranchByBrand');
Route::get('/area/{area}/branches','IndustryController@getBranchByArea');
Route::get('/brand/{brand}/area/{area}/branches','IndustryController@getBranchByBrandAndArea');
Route::get('/branch/{branch}/stands','IndustryController@getStand');

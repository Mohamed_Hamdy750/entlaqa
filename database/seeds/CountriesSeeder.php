<?php

use Illuminate\Database\Seeder;

class CountriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Geography\Country::class, 3)->create()->each(function ($country) {
            factory(App\Models\Geography\City::class, 3)->create(['country_id' => $country->id])->each(function ($city) {
                $areas = factory(App\Models\Geography\Area::class, 3)->make();
                $city->areas()->saveMany($areas);
            });
        });
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Models\Geography\Area;
use App\Http\Resources\Geography\Areas;

class CompaniesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $areas = Areas::collection(Area::all());

        factory(App\Models\Industry\Company::class, 3)->create()->each(function ($company) use ($areas) {
                factory(App\Models\Industry\Brand::class, 3)->create(['company_id' => $company->id])->each(function ($brand) use ($areas) {
                    foreach ($areas as $area) {
                        factory(App\Models\Industry\Branch::class, 2)->create(['brand_id' => $brand->id, 'area_id' => $area->id])->each(function ($branch) {
                            $stands = factory(App\Models\Industry\Stand::class, 3)->make();
                            $branch->stands()->saveMany($stands);
                        });
                    }
                });
        });
    }
}

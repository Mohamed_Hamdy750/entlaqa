<?php

use Faker\Generator as Faker;
use App\Models\Geography\Country;
use App\Models\Geography\City;
use App\Models\Geography\Area;
use App\Models\Industry\Company;
use App\Models\Industry\Stand;
use App\Models\Industry\Branch;
use App\Models\Industry\Brand;

$factory->define(Country::class, function (Faker $faker) {
    return [
        'name' => $faker->country,
    ];
});

$factory->define(City::class, function (Faker $faker) {
    return [
        'name' => $faker->city,
    ];
});

$factory->define(Area::class, function (Faker $faker) {
    return [
        'name' => $faker->state,
    ];
});

$factory->define(Company::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
    ];
});

$factory->define(Brand::class, function (Faker $faker) {
    return [
        'name' => $faker->companySuffix,
    ];
});

$factory->define(Branch::class, function (Faker $faker) {
    return [
        'name' => $faker->citySuffix,
    ];
});

$factory->define(Stand::class, function (Faker $faker) {
    return [
        'name' => $faker->domainWord,
    ];
});
